package com.serenegiant.usbcameratest3;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.CvType;
import org.opencv.android.Utils;
import org.opencv.core.*;
import org.opencv.android.OpenCVLoader;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by RaiMunoz on 5/8/16.
 */
public class image_binarization {

    private final static String TAG="image_binarization";

    // static {
    //     OpenCVLoader.initDebug();
    // }

    public Mat Resize(Mat mat_read) {
        Mat mat_r1 = new Mat();
        Size sz_r1 = new Size(1280, 960);
        Imgproc.resize(mat_read, mat_r1, sz_r1);
        return mat_r1;
    }

    public Mat magnify(Mat mat_r1)
    {
        Rect ROI = new Rect(106,80,1067,800);
        Mat mat_resize=mat_r1.submat(ROI);
        return mat_resize;
    }

    public Bitmap binarization(String img_name)
    {

        File imgFile = new File(img_name);
        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        Mat mat_read = new Mat(myBitmap.getWidth(), myBitmap.getHeight(), CvType.CV_8UC4);
        Utils.bitmapToMat(myBitmap, mat_read);
        Log.d(TAG, "The Image loaded!");

        Mat mat_rsize = new Mat(mat_read.width(), mat_read.height(), CvType.CV_8UC4);
        mat_rsize=Resize(mat_read);
        Mat mat = new Mat(mat_rsize.width(), mat_rsize.height(), CvType.CV_8UC4);
        mat = magnify(mat_rsize);
        Log.d(TAG, "The Image loaded and ready!");

        //Complements the Image
        Imgproc.cvtColor(mat,mat,Imgproc.COLOR_BGR2GRAY);
        Mat invertcolormatrix= new Mat(mat.rows(),mat.cols(), mat.type(), new Scalar(255,255,255));
        Core.subtract(invertcolormatrix, mat, mat);

        //Performs the morphological filter to erase any noise in the image
        int morph_size = 3;
        Mat matback=Mat.eye( mat.rows(),mat.cols(), CvType.CV_8UC1 );
        Mat element = Imgproc.getStructuringElement( Imgproc.MORPH_ELLIPSE, new Size( 4*morph_size + 1, 2*morph_size+1 ), new Point( morph_size, morph_size ) );
        Imgproc.erode(mat, matback, element);
        Core.subtract(mat, matback, mat);

        //I'm using the opening technique in this case and I'm allowing the image
        //to improve its resolution by filling in the areas that were not supposed to be erased by the erosion

        Imgproc.dilate(mat, matback, element);

        //equalizing everything to make sure that the image is normalized, i.e, better consistency in image resolution
        Imgproc.equalizeHist(mat, mat);
        Core.subtract(invertcolormatrix, mat, mat);
        Imgproc.blur(mat, mat, new Size(4, 4));


        //Converting the image to Binary
        Imgproc.threshold(mat, mat, 40, 255, Imgproc.THRESH_BINARY);
        Log.d(TAG, "The Binary image Complete!");


        Bitmap img_result = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mat, img_result);

        //Saving image as png
        //ByteArrayOutputStream stream = new ByteArrayOutputStream();


        try {
            final BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(imgFile));
            img_result.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.close();
        }
        catch(IOException e) {
            Log.e(TAG,"Error, could not save the image properly!");
        }

        return img_result;
    }


}
