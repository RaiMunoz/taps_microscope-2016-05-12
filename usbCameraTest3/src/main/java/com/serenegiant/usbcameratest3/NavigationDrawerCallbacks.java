package com.serenegiant.usbcameratest3;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
