package com.serenegiant.usbcameratest3;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;

import java.io.File;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Camera.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Camera#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Camera extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String TAG = "camera";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private  ProgressDialog pd;

    private  ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarbHandler = new Handler();
    private int fileSize = 0;

    public static final int CAMERA_REQUEST = 6986;
    public static final int SNAP_SHOT_RESULT = 22;
    public static final int SNAP_SHOT_CANCELED = 69;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    //Pubnub pubnub = new Pubnub("pub-c-fd510598-85f0-4c44-bd8f-dfdd12416064", "sub-c-61c25e6a-c9e9-11e5-b684-02ee2ddab7fe");
    Pubnub pubnub = new Pubnub("pub-c-b364700f-cf79-4ccd-95a7-880dfb4a2ebb", "sub-c-af52043a-05af-11e6-a6dc-02ee2ddab7fe");

    public Camera() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Camera.
     */
    // TODO: Rename and change types and number of parameters
    public static Camera newInstance(String param1, String param2) {
        Camera fragment = new Camera();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_camera, container, false);
        progressBar = new ProgressDialog(getActivity());
        progressBar.setCancelable(true);
        progressBar.setMessage("Getting result back from cloud ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(420);

        try {
            pubnub.subscribe("my_channel", new Callback() {

                @Override
                public void connectCallback(String channel, Object message) {
                    System.out.println("SUBSCRIBE : CONNECT on channel:" + channel
                            + " : " + message.getClass() + " : "
                            + message.toString());
                }

                @Override
                public void disconnectCallback(String channel, Object message) {
                    System.out.println("SUBSCRIBE : DISCONNECT on channel:" + channel
                            + " : " + message.getClass() + " : "
                            + message.toString());
                }

                public void reconnectCallback(String channel, Object message) {
                    System.out.println("SUBSCRIBE : RECONNECT on channel:" + channel
                            + " : " + message.getClass() + " : "
                            + message.toString());
                }

                @Override
                public void successCallback(String channel, Object message) {
                    System.out.println("SUBSCRIBE : " + channel + " : "
                            + message.getClass() + " : " + message.toString());
                    if (isNumeric(message.toString())) {
                        progressBar.dismiss();
                        System.out.println("data received: progress bar dismissed");
                        Intent navigation = new Intent(getActivity(), test_graph.class);
                        navigation.putExtra("mydata", message.toString());
                        startActivity(navigation);
                    }
                }

                @Override
                public void errorCallback(String channel, PubnubError error) {
                    System.out.println("SUBSCRIBE : ERROR on channel " + channel
                            + " : " + error.toString());
                }
            });
        } catch (PubnubException e) {
            e.printStackTrace();
        }

        Button takePic = (Button)v.findViewById(R.id.take_picture);
        takePic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String fileName = "Image_Sample.png";
                String folder_main = "USBCameraTest";
                File f = new File(Environment.getExternalStorageDirectory(), folder_main);
                if (!f.exists()) {
                    f.mkdirs();
                }
                String root = Environment.getExternalStorageDirectory().toString();

                File outputfile = new File(root + "/" + folder_main, fileName);
                Uri outputFileUri = Uri.fromFile(outputfile);

                Intent cameraIntent = new Intent(getActivity(),MainActivity.class);
                startActivityForResult(cameraIntent,1);

                //Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                //startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        return  v;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Intent snapshot = new Intent(getActivity(), SnapShot.class);
                snapshot.putExtra("name", "he_slide.png");
                startActivityForResult(snapshot, SNAP_SHOT_RESULT);
            }
        }

        if (requestCode == SNAP_SHOT_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                String info = data.getData().toString();
                if (info.equals("sent")) {
                    System.out.println("data status: " + info);
                    progressBar.show();
                    new CountDownTimer(420000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            //this will be done every 1000 milliseconds ( 1 seconds )
                            int progress = (int) ((420000 - millisUntilFinished) / 1000);
                            progressBar.setProgress(progress);
                        }

                        @Override
                        public void onFinish() {
                            //the progressBar will be invisible after 60 000 miliseconds ( 1 minute)
                            progressBar.dismiss();
                        }
                    }.start();
                } else if(info.equals("cancelled")) {
                    System.out.println("data status: " + info);
                } else {}
            }
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void take_picture(View view) {
        Intent snap = new Intent(getActivity(), SnapShot.class);
        getActivity().startActivity(snap);
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }
}
