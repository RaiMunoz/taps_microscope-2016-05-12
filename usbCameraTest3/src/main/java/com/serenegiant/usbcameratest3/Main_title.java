package com.serenegiant.usbcameratest3;;

import android.content.Intent;
import android.app.Activity;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Button;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.serenegiant.usbcameratest3.MainActivity;
//import com.example.william.taps_basic.R;

public class Main_title extends Activity {

    Button picbutton;
    Intent intent;
    static int STATIC_INT_LOG=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_title);

        picbutton=(Button)findViewById(R.id.startupbutton);

        picbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view){
                intent =new Intent(Main_title.this ,MainActivity.class);
                startActivity(intent);
                //finish();
            }

        });

    }

    // public void commence_page(View view) {
    //     Intent go_to_snapshot_page = new Intent(this,SnapshotPage.class);
    //     startActivity(go_to_snapshot_page);
    // }
}
