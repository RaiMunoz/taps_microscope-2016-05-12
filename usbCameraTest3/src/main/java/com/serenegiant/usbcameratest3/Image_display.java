package com.serenegiant.usbcameratest3;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.serenegiant.usb.CameraDialog;

import java.io.FileInputStream;
import java.io.File;
public class Image_display extends Activity {

    Button Continue;
    Button Retake;
    Intent in1;
    private ProgressDialog pad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_display);

        final String imageInSD = "/storage/emulated/0/DCIM/USBCameraTest/Image_Sample.png";
        Bitmap bitmap = BitmapFactory.decodeFile(imageInSD);
        //byte[] byteArray = getIntent().getByteArrayExtra("image");

        //Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        ImageView myImageView = (ImageView) findViewById(R.id.imageView_check);
        myImageView.setImageBitmap(bitmap);
        Button Continue = (Button) findViewById(R.id.continue_button);
        Button Retake = (Button) findViewById(R.id.retake_button);
    //}
        Retake.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                File file = new File(imageInSD);
                file.delete();
                in1=new Intent(Image_display.this, MainActivity.class);
                startActivity(in1);
                //setResult(Activity.RESULT_OK);
                Image_display.this.finish();
            }
        });
       // Continue.setOnClickListener(mOnClickListener);
       Continue.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
               pad = ProgressDialog.show(Image_display.this,"Please Wait",
                       "Sending Image");
               // startActivity(new Intent(Image_display.this, MainActivity.class));
           }
       });

    }


    public class RestartTask extends AsyncTask<Void, Void, Boolean> {
        private Activity activity;
        private ProgressDialog pd;

        public RestartTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            pd = ProgressDialog.show(activity, "Please Wait",
                    "Restarting Camera Interface...");
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            try {
                //activity.startActivity(new Intent(activity, MainActivity.class));
                //Image_display.this.finish();
                Thread.sleep(10000000);
            } catch (InterruptedException e) {
            }
            pd.dismiss();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            Toast.makeText(activity, Boolean.toString(result), Toast.LENGTH_LONG).show();
            //mHandler.captureStill();
            activity.startActivity(new Intent(activity, MainActivity.class));
            Image_display.this.finish();
        }
    }



    /*private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            switch (view.getId()) {
                case R.id.continue_button:
                        //Insert new Intent class here.
                    break;
                case R.id.retake_button:
                    //new RestartTask(Image_display.this).execute();
                    //Image_display.this.finish();
                     in1 = new Intent(Image_display.this, MainActivity.class);
                     startActivity(in1);
                    //Image_display.this.finish();

                    break;
            }
        }
    };
*/
    @Override
    protected void onStart()
    {
        super.onStart();
        Log.d("HomeScreenActivity","onStart method called up");

    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.d("HomeScreenActivity","onRestart method called up");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.d("HomeScreenActivity","onResume method called up");
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        Log.d("HomeScreenActivity","onPause method called up");
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        Log.d("HomeScreenActivity","onStop method called up");
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.d("HomeScreenActivity", "onDestroy method called up");
    }

}
