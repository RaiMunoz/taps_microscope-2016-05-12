package com.serenegiant.usbcameratest3;

import android.app.Fragment;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class Navigation extends AppCompatActivity
        implements NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_navigation);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        TextView titleText = (TextView) findViewById(R.id.mytext);
        titleText.setText("Camera");
        titleText.setGravity(Gravity.CENTER);
        setSupportActionBar(mToolbar);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);
        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        // populate the navigation drawer
        mNavigationDrawerFragment.setUserData("User", "", BitmapFactory.decodeResource(getResources(), R.drawable.avatar));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new Camera();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack(null)
                        .commit();
                break;
            case 1:
                TextView titleText1 = (TextView) findViewById(R.id.mytext);
                titleText1.setText("H&E Slides");
                titleText1.setGravity(Gravity.CENTER);
                fragment = getFragmentManager().findFragmentByTag(H_E_Slides.TAG);
                if (fragment == null) {
                    fragment = new H_E_Slides();
                }
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, fragment, H_E_Slides.TAG)
                        .addToBackStack(null)
                        .commit();
                break;
            case 2:
                TextView titleText2 = (TextView) findViewById(R.id.mytext);
                titleText2.setText("Results");
                titleText2.setGravity(Gravity.CENTER);
                fragment = getFragmentManager().findFragmentByTag(Results.TAG);
                if (fragment == null) {
                    fragment = new Results();
                }
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, fragment, Results.TAG)
                        .addToBackStack(null)
                        .commit();
                break;
            case 3:
                TextView titleText3 = (TextView) findViewById(R.id.mytext);
                titleText3.setText("Help");
                titleText3.setGravity(Gravity.CENTER);
                fragment = getFragmentManager().findFragmentByTag(Help.TAG);
                if (fragment == null) {
                    fragment = new Help();
                }
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, fragment, Help.TAG)
                        .addToBackStack(null)
                        .commit();
                break;
            case 4:
                finish();
                //Intent MainActivity = new Intent(Navigation.this,MainActivity.class);
                //startActivity(MainActivity);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.navigation, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
