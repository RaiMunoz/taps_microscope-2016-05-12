package com.serenegiant.usbcameratest3;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.io.File;
import java.net.URISyntaxException;

public class SnapShot extends AppCompatActivity {
    private static final String TAG = "UploadActivity";
    public static final String COGNITO_POOL_ID = "us-east-1:63f2c1d7-e29a-4d6b-8315-62a04e097332";
    public static final String BUCKET_NAME = "source-folder";
    private TransferUtility sTransferUtility;

    public static final int GET_IMAGE = 69;

    private  ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarbHandler = new Handler();
    private int fileSize = 0;
    private Button l;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");

                    // Load native library after(!) OpenCV initialization
                    //System.loadLibrary("ndklibrarysample");
                    // System.loadLibrary("ndklibrarysample");
                    // mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snap_shot);

        //Loading opencv
        if (!OpenCVLoader.initDebug()) {
            Log.e(this.getClass().getSimpleName(), "  OpenCVLoader.initDebug(), not working.");
        } else {
            Log.d(this.getClass().getSimpleName(), "  OpenCVLoader.initDebug(), working.");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        android.support.v7.app.ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.snapshot_navbar, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.snap_shot_text);
        mTitleTextView.setText("Camera");
        mTitleTextView.setTypeface(null, Typeface.BOLD);
        mTitleTextView.setGravity(Gravity.CENTER);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        String path = "/storage/emulated/0/DCIM/USBCameraTest/he_slide.png";//Environment.getExternalStorageDirectory() + "/photofolder/he_slide.jpg";
        File imgFile = new File(path);
        if (imgFile.exists()) {
            image_binarization start= new image_binarization();
            Bitmap myBitmap=start.binarization(path);
            //Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            ImageView myImage = (ImageView) findViewById(R.id.imageView2);
            if (myImage != null) {
                myImage.setImageBitmap(myBitmap);
            }
        } else {
            Toast.makeText(this, "IMAGE CANNOT BE DISPLAYED'", Toast.LENGTH_SHORT).show();
        }

        l = (Button) findViewById(R.id.send_to_cloud);

        if (l != null) {
            l.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CognitoCachingCredentialsProvider sCredProvider = new CognitoCachingCredentialsProvider(
                            getApplicationContext(),
                            COGNITO_POOL_ID,
                            Regions.US_EAST_1);

                    AmazonS3Client sS3Client = new AmazonS3Client(sCredProvider);

                    sTransferUtility = new TransferUtility(sS3Client,
                            getApplicationContext());
                    String path = "/storage/emulated/0/DCIM/USBCameraTest/he_slide.png";//Environment.getExternalStorageDirectory() + "/photofolder/he_slide.jpg";
                    File imgFile = new File(path);
                    Uri uri = Uri.fromFile(imgFile);
                    String newpath = null;
                    try {
                        newpath = getPath(uri);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    beginUpload(newpath);

                    progressBar = new ProgressDialog(v.getContext());
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Sending picture to cloud ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    progressBarStatus = 0;
                    fileSize = 0;
                    new Thread(new Runnable() {
                        public void run() {
                            while (progressBarStatus < 100) {
                                progressBarStatus = fileSize;

                                try {
                                    Thread.sleep(1000);
                                }

                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                progressBarbHandler.post(new Runnable() {
                                    public void run() {
                                        progressBar.setProgress(progressBarStatus);
                                    }
                                });
                            }

                            if (progressBarStatus >= 100) {
                                try {
                                    Thread.sleep(2000);
                                }

                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                progressBar.dismiss();
                                Intent data = new Intent();
                                String text = "sent";
                                data.setData(Uri.parse(text));
                                setResult(RESULT_OK, data);
                                finish();
                            }
                        }
                    }).start();
                }
            });
        }
    }
    /*
* Begins to upload the file specified by the file path.
*/
    private void beginUpload(String filePath) {
        if (filePath == null) {
            Toast.makeText(this, "Could not find the filepath of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }
        final File file = new File(filePath);
        TransferObserver observer = sTransferUtility.upload(BUCKET_NAME, file.getName(),
                file);
        /*
        Toast.makeText(this, "File is not null",
                Toast.LENGTH_LONG).show();
                */
        System.out.println("file name is: " + file.getName());
        System.out.println("file path is: " + filePath);
        System.out.println("file is: " + file);
        observer.setTransferListener(new TransferListener(){

            @Override
            public void onStateChanged(int id, TransferState state) {
                // do something
                /*
                Toast.makeText(getApplicationContext(), "onStateChanged",
                        Toast.LENGTH_LONG).show();
                        */
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percentage = (int) (bytesCurrent/bytesTotal * 100);
                fileSize = percentage;
                //Display percentage transferred to user
                /*
                Toast.makeText(getApplicationContext(), "onProgressChanged",
                        Toast.LENGTH_LONG).show();
                        */
                System.out.println("percentage: "+percentage);
            }

            @Override
            public void onError(int id, Exception ex) {
                // do something
                /*
                Toast.makeText(getApplicationContext(), "onError",
                        Toast.LENGTH_LONG).show();
                        */
            }

        });
        /*
         * Note that usually we set the transfer listener after initializing the
         * transfer. However it isn't required in this sample app. The flow is
         * click upload button -> start an activity for image selection
         * startActivityForResult -> onActivityResult -> beginUpload -> onResume
         * -> set listeners to in progress transfers.
         */
        // observer.setTransferListener(new UploadListener());
    }
    /*
 * Gets the file path of the given Uri.
 */
    @SuppressLint("NewApi")
    private String getPath(Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[] {
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.snap_shot, menu);
        return true;
    }


    @Override
    public void onPause()
    {
        super.onPause();
        // if (mOpenCvCameraView != null)
        //    mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        // if (mOpenCvCameraView != null)
        //    mOpenCvCameraView.disableView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_name) {
            Intent data = new Intent();
            String text = "cancelled";
            data.setData(Uri.parse(text));
            setResult(RESULT_OK, data);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
